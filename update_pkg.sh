#!/bin/bash

echo "Searching for updates..."
vscode_url=`curl -Ls -o /dev/null -w %{url_effective} -I 'https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-arm64'`
vscode_version=`echo $vscode_url | grep -oP "(?<=code_)[0-9.]+(?=-[0-9]+_arm64.deb)"`
vscode_commit=`echo $vscode_url | grep -oP "(?<=stable\/)\w+"`
vscode_commit_number=`echo $vscode_url | grep -oP "(?<=${vscode_version}-)[0-9]+(?=_arm64.deb)"`

pkgver=$(grep "pkgver=" PKGBUILD | cut -d "=" -f 2-) # Get current version from PKGBUILD

echo "Current Version: $pkgver"
echo "Latest Version: $vscode_version"

if [ "$pkgver" != "$vscode_version" ]; then
    echo "Updating PKGBUILD..."
    sed -i "s/pkgver=.*/pkgver=$vscode_version/" PKGBUILD
    sed -i "s/_commit=.*/_commit=$vscode_commit/" PKGBUILD
    sed -i "s/_commitnumber=.*/_commitnumber=$vscode_commit_number/" PKGBUILD
    echo "Updating checksums..."
    echo "Downloading new deb file..."
    wget -q $vscode_url
    sed -i "s|md5sums=(.*)|md5sums=('$(md5sum $(basename $vscode_url) | awk '{print $1}')')|" PKGBUILD
    rm "code_${vscode_version}-${vscode_commit_number}_arm64.deb"
    echo "Done!"
else
    echo "No updates available."
fi
